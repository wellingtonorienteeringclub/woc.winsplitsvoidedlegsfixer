﻿using CommandLine;

namespace WOC.WinsplitsVoidedLegsFixer {
  class Program {
    static void Main(string[] args) {
      Parser.Default.ParseArguments<Options>(args)
       .WithParsed<Options>(o => {
         var p = new Processor();
         p.Process(o);
       });
    }
  }
}