﻿using System;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class Helper {
    public static int? ReadInt(string message) {
      Console.WriteLine(message);
      string rawId = Console.ReadLine();

      if (Int32.TryParse(rawId, out int newId)) {
        return newId;
      }

      return null;
    }
  }
}
