﻿namespace WOC.WinsplitsVoidedLegsFixer {
  public class ResultListLeg {
    public string Start { get; set; }
    public string End { get; set; }
  }
}