﻿using System;
using System.Linq;
using IOF.XML.V3;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class ResultFileProcessor {
    public ResultList Process(ResultList resultList, ResultListProcessOptions resultListProcessOptions) {
      foreach (var classResult in resultList.ClassResult) {
        var sortClass = false;

        foreach (var personResult in classResult.PersonResult) {
          foreach (var result in personResult.Result) {
            double adjust = 0;

            for (int i = 1; i < result.SplitTime.Length; i++) {
              var c1 = result.SplitTime[i - 1];
              var c2 = result.SplitTime[i];

              if (c1.TimeSpecified && c2.TimeSpecified) {
                if (resultListProcessOptions.Legs.Any(p => p.Start == c1.ControlCode && p.End == c2.ControlCode)) {
                  sortClass = true;
                  double legTime = c2.Time - adjust - c1.Time;
                  adjust += legTime;
                  result.SplitTime[i].Time = result.SplitTime[i].Time - adjust + 0.1d;
                  Console.WriteLine($"Fixing leg {c1.ControlCode} - {c2.ControlCode} for {personResult.Person.Name.Given} {personResult.Person.Name.Family} removing {legTime}s.");
                } else {
                  result.SplitTime[i].Time = result.SplitTime[i].Time - adjust;
                }
              }
            }

            if (result.TimeSpecified) {
              var old = result.Time;
              result.Time -= adjust;
              Console.WriteLine($"Adjusting finish time for {personResult.Person.Name.Given} {personResult.Person.Name.Family} removing {adjust}s. From {old} to {result.Time}.");
            }
          }
        }

        if (sortClass) {
          Console.WriteLine($"Sorting class {classResult.Class.Name}.");

          classResult.PersonResult = classResult.PersonResult.OrderBy(p => {
            return p.Result != null && p.Result.Length > 0 && p.Result[0].Status == ResultStatus.OK && p.Result[0].TimeSpecified ? p.Result[0].Time : double.MaxValue;
          }).ToArray();

          int previousPlace = 0;
          int offset = 0;
          double previousTime = 0;

          foreach (var pr in classResult.PersonResult.Where(p => p.Result != null && p.Result.Length > 0)) {
            if (pr.Result[0].Status == ResultStatus.OK && pr.Result[0].TimeSpecified) {
              if (pr.Result[0].Time == previousTime) {
                offset++;
              } else {
                previousPlace += 1 + offset;
                offset = 0;
              }

              if (pr.Result[0].Position != previousPlace.ToString()) {
                Console.WriteLine($"Updating position for {pr.Person.Name.Given} {pr.Person.Name.Family} change from {pr.Result[0].Position} to {previousPlace}.");
              }

              pr.Result[0].Position = previousPlace.ToString();
            }
          }
        }
      }

      return resultList;
    }
  }
}