﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class XmlSerilizer {
    public T Deserialize<T>(string filename) where T : class {
      Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

      XmlSerializer serializer = new XmlSerializer(typeof(T));

      using (FileStream fs = new FileStream(filename, FileMode.Open)) {
        XmlReader reader = XmlReader.Create(fs);
        return serializer.Deserialize(reader) as T;
      }
    }

    public string Serialize<T>(T value) {
      if (value == null) {
        return null;
      }

      StringBuilder sb = new StringBuilder();

      try {
        XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
        using (XmlWriter writer = XmlWriter.Create(sb)) {
          xmlserializer.Serialize(writer, value);
          writer.Close();
        }

        return XDocument.Parse(sb.ToString()).ToString();
      } catch (Exception ex) {
        Console.WriteLine(ex);
        return null;
      }
    }
  }
}