﻿using System.Collections.Generic;
using CommandLine;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class Options {
    [Option('i', "input", Required = true, HelpText = "Path to input file.")]
    public string Input { get; set; }

    [Option('o', "output", Required = true, HelpText = "Path to output file.")]
    public string Output { get; set; }

    [Option('l', "leg", Required = true, HelpText = "Legs to be voided {controlNumber}-{controlNumber} i.e. 101-102.")]
    public IEnumerable<string> Legs { get; set; }
  }
}