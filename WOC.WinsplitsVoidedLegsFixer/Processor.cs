﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IOF.XML.V3;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class Processor {
    private readonly XmlSerilizer _XmlSerilizer;

    public Processor() {
      _XmlSerilizer = new XmlSerilizer();
    }

    public void Process(Options o) {
      var options = new ResultListProcessOptions {
        Legs = o.Legs.Select(p => {
          var segments = p.Split('-');
          return new ResultListLeg { Start = segments[0], End = segments[1] };
        }).ToList()
      };

      Console.WriteLine($"Input file is {o.Input}.");
      Console.WriteLine($"Output file is {o.Input}.");

      foreach (var leg in options.Legs) {
        Console.WriteLine($"Removing leg {leg.Start} - {leg.End}.");
      }

      ResultList resultList = null;

      if (File.Exists(o.Input)) {
        try {
          resultList = _XmlSerilizer.Deserialize<ResultList>(o.Input);
        } catch (Exception ex) {
          Console.WriteLine(ex);
          resultList = null;
        }
      }

      if (resultList == null) {
        Console.WriteLine($"Cannot find result file {o.Input}.");
      } else {
        resultList = new ResultFileProcessor().Process(resultList, options);
        File.WriteAllText(o.Output, _XmlSerilizer.Serialize(resultList));
      }

      Console.WriteLine("Done!");
    }

    public void Process2() {
      var legs = new List<ResultListLeg>();

      while (true) {
        int? startControlNumber = Helper.ReadInt("Control number for start of leg?");

        if (!startControlNumber.HasValue) {
          break;
        }

        int? endControlNumber = Helper.ReadInt("Control number for end of leg?");

        if (!endControlNumber.HasValue) {
          break;
        }

        legs.Add(new ResultListLeg { Start = startControlNumber.Value.ToString(), End = endControlNumber.Value.ToString() });
      }

      string filePath = @"D:\nationals2020\sprint-splits-novoid.xml";

      ResultList resultList = null;

      if (File.Exists(filePath)) {
        try {
          resultList = _XmlSerilizer.Deserialize<ResultList>(filePath);
        } catch (Exception ex) {
          Console.WriteLine(ex);
          resultList = null;
        }
      }

      if (resultList == null) {
        Console.WriteLine($"Cannot find result file {filePath}.");
      } else {
        resultList = new ResultFileProcessor().Process(resultList, new ResultListProcessOptions { Legs = legs });
        File.WriteAllText(filePath.Replace(".xml", "_updated.xml"), _XmlSerilizer.Serialize(resultList));
      }

      Console.WriteLine("Done!");
    }
  }
}