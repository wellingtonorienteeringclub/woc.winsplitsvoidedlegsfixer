﻿using System.Collections.Generic;

namespace WOC.WinsplitsVoidedLegsFixer {
  public class ResultListProcessOptions {
    public List<ResultListLeg> Legs { get; set; }
  }
}